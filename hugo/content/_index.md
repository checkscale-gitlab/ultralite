---
title: "[ultralite]# dev|it|blog"
---
{{< caption-lg src="https://cdn.dribbble.com/users/948894/screenshots/2381290/dribbbleglide.gif" caption="[This gif is what comes to mind when I think of Ultralite](https://dribbble.com/shots/2381290-hang-gliding?utm_source=Clipboard_Shot&utm_campaign=andreasrusch&utm_content=hang%20gliding&utm_medium=Social_Share)" >}}

# What is Ultralite?
Ultralite is my personal philosophy and approach to technology. This approach
comes form the ultralite backpacking movement, something I really enjoyed
for a time, my wife and I. The basic tenet of it was purposeful planning and
eliminating things you just don't need, the result is going farther and doing
more because you have effectively lightened your load.

In the world of technology, I have found there is much joy to be found in
finding ways to go farther, so to speak, and reducing the weight by replacing
tools and systems with those that were designed with simplicity and utility.
Ultralite is not doing with less, it is *doing more with less*.

# Who is Ultralite?

{{< caption src="https://simpleicons.org/icons/gitlab.svg" caption="[Gitlab repo for Ultralite](https://gitlab.com/jptechnical/ultralite)" >}}My name is Jesse Perry, I go by JP, and I operate [JP
Technical](https://www.jptechnical.com), a small technology service business in
Anchorage Alaska. I work with businesses under 50 users, and provide support for
a wide array of technologies. I also work closely with a large non-profit where
we build systems at scale using the very latest in tools and technologies
following DevOps principles.

# Why is Ultralite?

I made this site as a place to write about things I learn as I continue to apply
DevOps principles to my business. I wanted a place to demonstrate how simple and
yet how powerful these light-weight ideals can be. I am using this [Gitlab repo
for Ultralite](https://gitlab.com/jptechnical/ultralite) as a little bit of a
showcase for some small projects. For instance, this blog is running on AWS
S3, CloudFront and is nearly infinitely scalable, but only costs me pennies a
month for the bandwidth. Until I get more of these projects committed to this
repo, this will mainly be a place I can keep tabs on little tips and tricks I
learn as the need arises. You can see these little (very minor) victories in [my
blog](/post).

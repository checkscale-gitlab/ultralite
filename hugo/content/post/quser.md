---
title: Remotely log off user
author: Jesse Perry
date: "2021-03-31"
categories:
- HowTos
tags:
- CMD
- WINDOWS
draft: no
---
# How To Remotely Log Off a User With `QUser` and `Logoff`

While installing an update to QuickBooks, I found that the installer would fail
because QuickBooks was open by another user. In this case, it was easier to
remotely log the user off than find the processes that were interfering. Here is
how you do it.

## Seeing who is logged in with `quser`

First, let's find all the logged in users with `quser`. Notice that the logged
in user named `Sally` is `ID 1` and is in the `STATE` of `disc`, which means
they are logged in but disconnected. Also you will see that my login session is
identified with the `>` prefix, to make it easy to tell who you are and you
don't log yourself off.

```batch
C:\WINDOWS\system32>quser
 USERNAME              SESSIONNAME        ID  STATE   IDLE TIME  LOGON TIME
 Sally                                     1  Disc           10  3/29/2021 12:13 PM
>jptechnical           console             2  Active         10  3/31/2021 4:49 PM
```

## Logging someone off with `logoff`

It's funny how often you find out a tool you have used for years does way more
than you realized. As an example, for years I have ended a windows session with
the command `logoff`. But I never actually looked to see what the command will
do. As it turns out, you can log OTHER PEOPLE off as well, noice! So now I can
`logoff` Sally by the ID number. I will run the `quser` command again and Sally is
no longer logged in.

```batch
C:\WINDOWS\system32>logoff 1

C:\WINDOWS\system32>quser
 USERNAME              SESSIONNAME        ID  STATE   IDLE TIME  LOGON TIME
>jptechnical           console             2  Active         13  3/31/2021 4:49 PM
```

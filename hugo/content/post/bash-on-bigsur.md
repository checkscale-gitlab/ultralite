---
title: Set the latest bash as the default terminal on MacOS Big Sur
author: Jesse Perry
date: "2021-02-28"
categories:
- HowTos
tags:
- CMD
- BASH
draft: no
---
# Setting modern Bash as the default terminal on MacOS Big Sur

For a variety of reasons, Apple has set ZSH as the default shell in the
terminal. Changing the default terminal is trivial enough with the `chsh`
command. However, the version of Bash that comes with MacOS is oooold, way old.
Fortunately it is also trivial to install the latest version of Bash. Let's
install the latest Bash and then set it as our default. 

## First verify you have the old version of Bash

Verify what version of Bash you have, you can run `echo $BASH_VERSION` to see
what version you are using. If it is less than version 5, it's old, let's
upgrade it. If the output is blank, you might need to run `bash` first.

## Install the latest Bash with Homebrew

If you aren't using Homebrew... you should, it takes a TON of headaches out of
using your mac. For instructions on how to install Homebrew, visit the website
at [brew.sh](https://brew.sh). Once installed, the syntax of homebrew is pretty
intuitive, `brew install bash` installs the latest version of Bash. See the
example output below. Once installed, running `bash` should open the newly
installed version. You should verify your version again with `echo $BASH_VERSION`

```bash
% brew install bash
==> Downloading https://homebrew.bintray.com/bottles/bash-5.1.4.big_sur.bottle.tar.gz
Already downloaded: /Users/jp/Library/Caches/Homebrew/downloads/dbf7ab25fb4943ebe4048f45cfc40bbeedc6a5d44d76f2546f9d672b9b9428b2--bash-5.1.4.big_sur.bottle.tar.gz
==> Pouring bash-5.1.4.big_sur.bottle.tar.gz
🍺  /usr/local/Cellar/bash/5.1.4: 157 files, 10.9MB
```

## Here is how you set Bash as your default shell. 

Let's set your shiny new Bash install as the default shell in MacOS. First of
all, let's run the `which bash` command to find out the path for our new version
of bash. On my system the path is `/usr/local/bin/bash`. 

With that path we can first make sure that this version of Bash is in the
`/etc/shells` file as an allowed shell. If it is not, you can add
`/usr/local/bin/bash` by editing `/etc/shells`. 

Finally, you will run the `chsh -s /usr/local/bin/bash` to set our new shell. 

If all goes well you will get bash when you open your terminal next. WOOT!

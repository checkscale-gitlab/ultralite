---
title: In search of the perfect VM
author: Jesse Perry
date: "2021-01-21"
categories:
- Ramblings
tags:
- VM
draft: no
---
# In my quest, I tended to walk in circles

Wise king Solomon said 'there is nothing new under the sun', when it comes to
computing that is often the case, things we think are brand new have already
been done before by early pioneers. [
Virtualization](https://en.wikipedia.org/wiki/Virtualization) is one such modern
miracle that was actually born in the 60s as a method of dividing resources in
mainframes.  There have been numerous renaissance periods; server virtualization
in the late 90s, VDI for desktops in the 2010s, and then [
Docker](https://en.wikipedia.org/wiki/Docker_(software)) in 2013 is still
reshaping computing today. I wasn't there for mainframes, but I was there for
the emergence of server virtualization.  

## Virtual Machines, the dawn of a new era 

I first started playing with [virtual machines
](https://en.wikipedia.org/wiki/System_virtual_machine) in 1999, with VMware
Workstation, bootleg of course... oh the 90s 😳. In 2004ish [Microsoft Virtual
PC](https://en.wikipedia.org/wiki/Windows_Virtual_PC) (and later [Virtual
Server](https://en.wikipedia.org/wiki/Microsoft_Virtual_Server) in 2005) came
out and it was pretty good, and a WHOLE LOT cheaper. But back then, running a
virtual server was kind of dicey, because [bare-metal hypervisors
](https://www.vmware.com/topics/glossary/content/bare-metal-hypervisor) didn't
come out until 2007, even then they were out of reach for the little guys. The
result was a semi-manual [VirtualBox](https://www.virtualbox.org/) service
running on a server or a desktop, you always had to be logged in and the
application running in the background. It wasn't incredibly stable, but it was
still mind blowingly cool that you can use so much under-utilized hardware and
split it into more systems. 

## Production ready directly from boot

So my journey to find the ultimate hypervisor took me to [VMware ESXi
](https://en.wikipedia.org/wiki/VMware_ESXi), the first FREE bare-metal
hypervisor, it was AMAZING! Finally I could boot from a CD, and in a few minutes
have a server dedicated to hosting VMs. Not long after this I discovered a tool
to complete a [p2v](https://en.wikipedia.org/wiki/Physical-to-Virtual) of a live
system called [VMware Converter
Standalone](https://my.vmware.com/en/web/vmware/downloads/info/slug/infrastructure_operations_management/vmware_vcenter_converter_standalone/6_2_0)...
WHILE IT WAS RUNNING. In fact, I remember in 2004ish a friend of mine in
Anchorage didn't believe me that you could p2v a running system, so we used [
RAdmin](https://www.radmin.com/press/releases/detail.php?ID=9) to setup a screen
share so he could watch, I think he even got a co-worker to watch, they were
blown away, and they had far more business IT Support experience than I had.
That was straight up amazing. I converted several legacy servers to a VM to
extend their useful lifespan, preemptively getting it off of laboring hardware
before it died. Nowadays, I typically use the tried and true [Sysinternals
Disk2VHD.exe](https://docs.microsoft.com/en-us/sysinternals/downloads/disk2vhd),
because it still works just fine, and these days I build far more virtual
systems than physical. In time I needed to do more advanced stuff, like backups
(you wouldn't think that would be hard but it was) and moving VMs from one host
to another, and that is where the free edition of ESXi showed it's rough
(expensive) edges. And so I began my wandering for a replacement, it needed to
be free and support these new objectives. 

## Open source with premium options is now a thing

  {{< caption src="/images/xenproject.png" caption="The Xen Project [powers some of the biggest clouds in the business](https://xenproject.org/users/why-xen/)" >}}
My next stop was Citrix XenServer (now called [Citrix
Hypervisor](https://www.citrix.com/products/citrix-hypervisor/)), this was also
available as a free product, with similar capabilities to VMware ESXi, but
having the opensource [Xen](https://en.wikipedia.org/wiki/Xen#XENSERVER), the
granddaddy of hypervisors first introduced in 2003, as it's underlying
hypervisor there were very many options. This was a really great hypervisor, the
console was really easy to use and it was the most stable OS I had used up until
this point.  In fact, I never replaced a system that failed, never had to rescue
one from some bad patch or something like that.  Sadly, Citrix changed their
licensing, removing features while increasing the price, so it was time to begin
wandering once again. 

## Microsoft surprises us all with a FREE edition

  {{< caption src="/images/hyper-v.png" caption="[Microsoft Hyper-V Server](https://www.microsoft.com/en-us/evalcenter/evaluate-hyper-v-server-2019) is a free bare-metal hypervisor" >}}
Around this time [Microsoft's Hyper-V](https://en.wikipedia.org/wiki/Hyper-V)
was gaining maturity. Beginning with Server 2012, a new edition called [Hyper-V
Server 2012](https://en.wikipedia.org/wiki/Hyper-V#Hyper-V_Server) was released,
FOR FREE, and it had all the bells and whistles that the edition running on
regular Windows Server 2012. This was killer, because Hyper-V was all
pointy-clicky in their very unified management console, and your Windows backup
tools worked (for the most part). What became the real deal maker was that you
could do replication between servers without extra cost, even without Active
Directory. So now you can have a hot standby of your critical services that sync
every 5min, just click a few buttons to transfer over to your spare in case of
failure. Surely this is the end of my search. But then, lo and behold came
[DevOps](https://aws.amazon.com/devops/what-is-devops/), and a calling to my
terminal and *nix roots, and there was finally a name for all the manual
repetitive the pointy-clicky, and that name was
[TOIL](https://www.rundeck.com/blog/toil-finally-a-name-for-a-problem) and I
began a campaign to banish it wherever it was found. And so I begand wandering
again... looking for the thing that supported my newly adopted DevOps ideals. 

## The opensource community rescues an old favorite
  {{< caption src="/images/xcp-ng.png" caption="[XCP-ng](https://xcp-ng.org) picks up where XenServer left off" >}}

And now, I have found the next big thing, as far as I am concerned. I have come
back to Xen, like that favorite pair of shoes, only they have new soles and
don't stink of greedy Citrix anymore. [XCP-NG](https://xcp-ng.org/) is an opensource edition of
XenServer, a fork of the classic, with an organization producing a new dashboard
called Xen Orchestra and options, OPTIONS I SAY, for support. You can have the
free and the paid in any combination you require.  What amazing new things have
caught my eye with the new XCP-NG and XO? Well, backup is a first class citizen,
with a backup facility baked in. Also, replication has become easier than ever,
two hosts in the same console can seamlessly migrate back and forth, it's quite
impressive. And now that multiple cores and tens of gigs of ram is readily
affordable, you can easily have three hosts instead of two, and if you put them
together in a pool you can have High Availability for your mission critical
systems; I am actively evaluating this, it's like the holy grail in all my
journeys.

And so I have ceased my wandering, for a time anyway. Who knows, maybe Virtual
PC will come back, free and bristling with replication and HA? 🤔 In the
meantime, I will *hopefully* stick with XCP-ng for a bit, and I'll make a HowTo on
it soon.

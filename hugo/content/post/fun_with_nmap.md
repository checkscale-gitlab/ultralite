---
title: Fun with nmap
author: Jesse Perry
date: "2021-03-30"
categories:
- HowTos
tags:
- BASH
- NETWORKING
draft: no
---
# Fun with nmap

Since 1997, nmap has been been an essential part of the sysadmin's toolbox.
There are a dizzing number of arguments you can use to change nmap's behavior
and output. Even after all these years, I still end up Googling for how to do
some fairly routine tasks with nmap. Here are a few of my most commonly used
commands, as well as a way to find more details from the docs.

## Quick ping sweep with nmap

The following command will scan the subnet and provide the output in an easy to
read format, then pipe it through `less` so it is easily searchable. Let's look
at that command and it's output.

```bash
$ nmap -sn 192.168.0.0/24 -oG nmap_output | less
Nmap 7.80 scan initiated Fri Mar  30 22:01:57 2021 as: nmap -sn -oG nmap_output 192.168.0.0/24
Host: 192.168.0.1 (Archer.lan)    Status: Up
Host: 192.168.0.101 (Lyric-1111C2.lan)    Status: Up
Host: 192.168.0.151 (SoundTouch-VW-benee.lan)    Status: Up
Host: 192.168.0.160 (SoundTouch-VW-keuken.lan)    Status: Up
Host: 192.168.0.181 ()    Status: Up
Host: 192.168.0.225 (TL-WPA4220.lan)    Status: Up
Host: 192.168.0.165 (f3d0r4.lan)    Status: Up
# Nmap done at Fri Mar  6 22:02:06 2020 -- 256 IP addresses (7 hosts up) scanned in 9.45 seconds
<END>
```

## Check for specific open ports

If we want to look for specific open ports, we can scan like this. We pipe it
through `less` and we can grep it from there.

```bash
$ nmap -sV -p 22,443 192.168.0.0/24 | less
Starting Nmap 7.91 ( https://nmap.org ) at 2021-03-30 15:38 AKDT
Nmap scan report for 192.168.0.1
Host is up (0.0051s latency).

PORT    STATE SERVICE  VERSION
22/tcp  open  ssh      OpenSSH 6.6.1p1 Debian 4~bpo70+1 (protocol 2.0)
443/tcp open  ssl/http Ubiquiti Edge router httpd
Service Info: OS: Linux; Device: router; CPE: cpe:/o:linux:linux_kernel

Nmap scan report for 192.168.0.10
Host is up (0.0030s latency).

PORT    STATE SERVICE  VERSION
22/tcp  open  ssh      OpenSSH 7.4 (protocol 2.0)
443/tcp open  ssl/http nginx
```

## I like my nmap like I like my burgers, 'with the works'

If you really want a lot of info on a host, then the `-A` argument will give you
the works. In our case below, we found a Synology NAS device that is running
`SMBv2` with guest enabled.  


```bash
$ nmap -A 192.168.0.10
Starting Nmap 7.91 ( https://nmap.org ) at 2021-03-30 15:45 AKDT
Nmap scan report for 192.168.0.10
Host is up (0.0039s latency).
Not shown: 990 closed ports
PORT     STATE SERVICE     VERSION
...
139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
...
Host script results:
|_clock-skew: mean: 2h19m59s, deviation: 4h02m32s, median: -2s
|_nbstat: NetBIOS name: SYNOLOGY, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb-os-discovery:
|   OS: Windows 6.1 (Samba 4.4.16)
|   Computer name: synology
|   NetBIOS computer name: NAS\x00
|   Domain name: \x00
|   FQDN: synology
|_  System time: 2021-03-30T16:45:20-07:00
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode:
|   2.02:
|_    Message signing enabled but not required

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 33.98 seconds
```

## Grepping for what a command argument means

If you want to know more about what a command means, you can grep for it. For
instance, to know what the `-sV` argument does, you can run the command below.

```bash
$ nmap | grep -- -sV
  -sV: Probe open ports to determine service/version info
```

If you are unsure about what the `--` is for, let's try that grep trick on bash
and see the output. 

```bash
$ man bash | grep -- --
       --        A  --  signals the end of options and disables further option
                 processing.  Any arguments after the -- are treated as  file-
                 names and arguments.  An argument of - is equivalent to --.
```

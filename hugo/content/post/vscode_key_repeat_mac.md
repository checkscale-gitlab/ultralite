---
title: Key repeat in VS Code on MacOS
author: Jesse Perry
date: "2021-03-08"
categories:
- HowTos
tags:
- MacOS
- VSCODE
draft: no
---

I recently rebuilt my Mac, I do this periodically as a habit from my Windows
days I haven't managed to break yet, but at least it is only once every major
version, instead of semi-yearly with Windows. The exercise is good for me,
though, because I find little tools and tweaks that I have forgotten to commit
to my ansible file. This one such example. 

While [NeoVim](https://neovim.io/) is my code editor of choice, I use [VS
Code](https://code.visualstudio.com) because it is the standard at the
non-profit I work with. Since I have become HOOKED on the workflow of Vim, I
have the awesome [Vim plugin](https://github.com/VSCodeVim/Vim) installed. I was
building a playbook for EKS upgrades and noticed this little irritation.

## What is the 'press and hold' feature on MacOS? 

There is a feature in MacOS for [typing accented letters and
diacritics](https://www.freecodecamp.org/news/how-to-type-letters-with-accents-on-mac)
that involves pressing and holding a key down. Since Vim uses `hjkl` as motion
keys, you often press and hold one of these motions as an, inefficient, way of
navigating. I *should* be using the motions more efficiently, like `10l` to go
to characters to the right, but <s>often</s> sometimes I am just too lazy. With
the 'press and hold' feature in MacOS, the result is trying to use characters
like `ł` instead of a long string of `l` characters moving me to the right.
Let's fix that. 

## Turning off 'press and hold' for VSCode

To turn off this feature, only for VSCode, you can run the following command in
your terminal. Then you will have to restart VSCode for this to take effect.

```bash
defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false
```

To re-enable this feture in VSCode, you can run the opposite command.

```bash
defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool true
```

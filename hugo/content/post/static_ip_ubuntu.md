---
title: Set a static ip in Ubuntu 18.04
author: Jesse Perry
date: "2021-03-05"
categories:
- HowTos
tags:
- UBUNTU
- NETWORKING
draft: no
---
# Setting a static IP address on Ubuntu 18.04 using Netplan

Netplan is the default network manager since Ubuntu 17.10. No longer do you edit
the `/etc/network/interfaces` file, ohh the memories. Now we get to work with
`YAML`, which is a nice change, and then the actual configuration is rendered
from this configuration file.

I have already ssh'd into my system, this time I used Ubuntu's ability to import
my saved public key from [launchpad.net](https://launchpad.net) based on my user
account, this was quite slick, I might try that again sometime. This system is
going to be for testing out [the Helk](https://thehelk.com), a pre-configured
and easily installable [ELK Stack](https://aws.amazon.com/elasticsearch-service/the-elk-stack/). 
 Let's jump in.

## Determine your interface device

First, I determine my current network device with the command `ip link`, see the
output below. The `eth0` is the virtual adapter (I am using [XCP-NG]({{< relref "/post/xcp-ng.md" >}})

```bash
jp@elk:~$ ip link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DEFAULT group default qlen 1000
    link/ether b6:7d:64:89:ce:9e brd ff:ff:ff:ff:ff:ff
```

## Edit the Netplan config

The config files for Netplan are in the `/etc/netplan` directory, but in my
installation there is no obvious network config, just the one created by the
installer called `00-installer-config.yaml`. So, I created one called
`01-netcfg.yaml`, since that is what I was expecting to see, then I renamed the
old one to get it out of the way. Finally, I ran `sudo netplan apply` to make
sure it worked. In the end, I could have just left the filename the same, or
renamed it, but sometimes we learn more from our mistakes than getting it right
the first time.

```bash
jp@elk:~$ sudo cp /etc/netplan/00-installer-config.yaml /etc/netplan/01-netcfg.yaml
[sudo] password for jp:
jp@elk:~$ sudo mv /etc/netplan/00-installer-config.yaml /etc/netplan/00-installer-config.yaml.bak
sudo netplan apply
... nothing bad happened! ¯\_(ツ)_/¯
```

Now we need to edit the plan, to do this I am using Vim, but you do you. Below
is what my unmodified Netplan looks like. 

```yaml
jp@elk:/etc/netplan$ cat 01-netcfg.yaml
# This is the network config written by 'subiquity'
network:
  ethernets:
    eth0:
      dhcp4: true
  version: 2
```

Let's jazz that up with some static goodness. Here is my config with a static
address. Then when I am reasonably sure this is good, I run `sudo netplan
apply`. 
```yaml
jp@elk:/etc/netplan$ cat 01-netcfg.yaml
# This is the network config written by 'JP'
network:
  ethernets:
    eth0:
      dhcp4: false
      addresses:
        - 10.1.1.70/24
      gateway4: 10.1.1.1
      nameservers:
        addresses:
          - 1.1.1.3
          - 1.0.0.3
  version: 2
```

Of course, my ssh session is immediately abandoned... I should have run
`screen` first. I was able to reconnect with the new IP address. So, it worked,
and now I have a static IP address on this lab system. NOICE!
